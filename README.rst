.. image:: https://gitlab.com/krishkasula/txtoflow/badges/master/pipeline.svg?style=flat-square
.. image:: https://img.shields.io/pypi/dm/txtoflow?label=downloads%20%28no%20mirrors%29&style=flat-square
.. image:: https://img.shields.io/pypi/dw/txtoflow?label=&style=flat-square
.. image:: https://img.shields.io/pypi/dd/txtoflow?label=&style=flat-square

Tx To Flow
----------

Translate To Flowchart

* Documentation is available `here <https://krishvk.gitlab.io/txtoflow/index.html>`_
* `txtoflow <https://gitlab.com/krishkasula/txtoflow>`_ is available on `PyPi <https://pypi.org/project/txtoflow/>`_

Installation
============

.. code::

    pip install txtoflow